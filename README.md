# PhiliBert

### Environment

The python packages used by this app can be found in the ```philibert_py_env.yml```file. This file can be used to create the needed environment for the app. By default the environment name is ```philibert```, you can this name in the yml file. After you chose a name, use those commands to create the environment and to activate it.

```shell
conda env create -f philibert.yml
conda activate philibert
```

### Development

If you want to contribute to this project and run each service independently:

#### Launch API

Then you'll have to type the following commands:

```shell
cd src/api/
python app.py
```

#### Launch Dash app

In order to run the `dash` server to visualize the output:

```shell
cd src/dash/
python app.py
```

#### Visualize the app

The app can be found in your web browser at the adress ```http://localhost:8050/```
