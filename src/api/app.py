import os
import pickle
import random
import sys

import fasttext
import numpy as np
import pandas as pd
from flask import Blueprint, Flask, jsonify, request
from tqdm import tqdm

import config
from utils import (
    clean_themes,
    cleantext,
    compute_cosine_similiraty,
    get_most_likely_themes,
    vectorize_sentence,
)

app = Flask(__name__)
api = Blueprint("api", __name__)

ft_model = fasttext.load_model(config.MODEL_PATH)

file = open(os.path.join(config.PATH_DATA, "le.obj"), "rb")
label_encoder = pickle.load(file)
file.close()

file = open(os.path.join(config.PATH_DATA, "model.obj"), "rb")
clf_model = pickle.load(file)
file.close()

games = pd.read_pickle(os.path.join(config.PATH_DATA, "philibert_fr_vectorized.pkl")).drop_duplicates(subset='title')


@api.route("/find_best_games", methods=["POST"])
def get_best_games():
    """
    Endpoint to predict the best games using the
    user's text data.
    """
    if request.method == "POST":
        if "review" not in request.form:
            return jsonify({"error": "no review in body"}), 400
        else:
            input = request.form["review"]
            input_cleaned = cleantext(str(input))
            input_vectorized = vectorize_sentence(input_cleaned, ft_model).reshape(
                1, -1
            )
            input_themes_prob = clf_model.predict_proba(input_vectorized)
            input_themes = get_most_likely_themes(
                input_themes_prob[0], encoder=label_encoder, nb_class=3
            )
            games["intersection"] = games["cleaned_themes"].apply(
                lambda x: len(list(set.intersection(set(x), set(input_themes))))
            )
            games_filtered = games[lambda x: x.intersection > 0]

            games_filtered["cosine"] = games_filtered["vectorized_description"].apply(
                compute_cosine_similiraty, user_sentence_vectorized=input_vectorized
            )

            best_indexes = (
                games_filtered.sort_values(by="cosine", ascending=False).head(3).index
            )
            output = (int(best_indexes[0]), int(best_indexes[1]), int(best_indexes[2]))
            return jsonify(output)


app.register_blueprint(api, url_prefix="/api")

if __name__ == "__main__":
    app.run(debug=config.DEBUG, host=config.HOST)
