import os

MODEL_PATH = "./pkl/wiki.fr.bin"
PATH_DATA = "./pkl/"
ENVIRONMENT = os.environ.get("ENVIRONMENT", "dev")
DEBUG = ENVIRONMENT == "dev"
HOST = '0.0.0.0' if ENVIRONMENT == "prod" else 'localhost'
