import itertools
import re

import cleantext as cltxt
import nltk
nltk.download('stopwords')
import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity


def cleantext(text):
    """
    Converts text information into lower case clean text, without extra_spaces, numbers and punctuation
    """
    if isinstance(text, list):
        text = " ".join(text)
    return cltxt.clean(
        text,
        extra_spaces=True,
        lowercase=True,
        numbers=True,
        punct=True,
        stopwords=True,
        stp_lang="french",
    )


def vectorize_sentence(string, model):
    """
    Converts text information into a vector
    """
    return np.array(model.get_sentence_vector(string))


def get_most_likely_themes(probs, encoder, nb_class=3):
    """
    Compute the most likely classes
    """
    classes = encoder.classes_
    return [x for _, x in sorted(zip(probs, classes), reverse=True)][0:nb_class]


def clean_themes(x):
    """
    Compute the intersection between predicted and
    """
    intersec = len(list(set.intersection(set(x.predicted_themes), set(x.themes))))
    if intersec == 0:
        cleaned_themes = x.predicted_themes
    else:
        cleaned_themes = list(set((x.predicted_themes + x.themes)))
    return cleaned_themes


def compute_cosine_similiraty(vectorized_string, user_sentence_vectorized=""):
    """
    Compute cosine_similarity between user sentence vector and an other one
    """
    return cosine_similarity(vectorized_string.reshape(1, -1), user_sentence_vectorized)
