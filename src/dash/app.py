import os
import time

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import pandas as pd
import requests
from dash.dependencies import Input, Output, State
from flask import request

import config

external_stylesheets = [
    "https://use.fontawesome.com/releases/v5.0.7/css/all.css",
    "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
    "https://fonts.googleapis.com/css?family=Roboto&display=swap",
]

external_script = (
    "https://gitlab.com/AlricGM/philibert/-/blob/master/src/dash/assets/gtag.js"
)

app = dash.Dash(
    __name__,
    external_stylesheets=external_stylesheets,
    meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}],
    suppress_callback_exceptions=True,
)

app.scripts.append_script({"external_url": external_script})

app.title = "PhiliBert"

games = pd.read_pickle(os.path.join(config.PATH_DATA, "philibert_fr_vectorized.pkl")).drop_duplicates(subset='title')

app.layout = html.Div(
    [dcc.Location(id="url", refresh=False), html.Div(id="page-content")]
)

home_layout = html.Div(
    [
        html.H2(
            ["Quel type de jeu recherchez-vous ?",],
            className="h4 mb-4 font-weight-normal",
        ),
        html.Div(
            [
                dcc.Textarea(
                    className="form-control z-depth-1",
                    id="review",
                    rows="6",
                    placeholder="Je recherche un jeu dans lequel...",
                )
            ],
            className="form-group shadow-textarea",
            style={"width": "600px", "display": "inline-block"},
        ),
        html.Div(
            [
                html.Div(
                    [
                        dcc.RangeSlider(
                            id="players_nb_range_slider",
                            marks={1: "1 joueur", 10: "10+ joueurs"},
                            min=1,
                            max=10,
                            step=1,
                            value=[1, 10],
                        ),
                        html.Div(
                            id="output_players_nb_range_slider",
                            style={
                                "marginTop": "40px",
                                "display": "inline-block",
                                "font-size": "13px",
                            },
                        ),
                    ],
                    className="form-group shadow-textarea",
                    style={
                        "width": "200px",
                        "display": "inline-block",
                        "marginRight": "50px",
                    },
                ),
                html.Div(
                    [
                        dcc.RangeSlider(
                            id="age_range_slider",
                            marks={1: "1 an", 14: "14+ ans"},
                            min=1,
                            max=14,
                            step=1,
                            value=[1, 14],
                        ),
                        html.Div(
                            id="output_age_range_slider",
                            style={
                                "marginTop": "40px",
                                "display": "inline-block",
                                "font-size": "13px",
                            },
                        ),
                    ],
                    className="form-group shadow-textarea",
                    style={
                        "width": "200px",
                        "display": "inline-block",
                        "marginLeft": "50px",
                    },
                ),
            ],
            style={"display": "inline-block", "vertical-align": "top"},
        ),
        html.Button(
            [
                html.Span("Go", style={"marginRight": "10px",}),
                html.I(className="fa fa-search"),
            ],
            className="btn btn-lg btn-primary btn-block",
            role="submit",
            id="submit_button",
            style={"width": "200px", "margin": "auto"},
        ),
        html.Div(
            [
                html.Div(
                    [
                        html.Div(
                            [
                                html.A(
                                    html.Img(
                                        id="game_logo0",
                                        style={"height": "150px", "padding": "20px"},
                                    ),
                                    id="game_url0",
                                    target="_blank",
                                )
                            ],
                            style={
                                "display": "inline-block",
                                "height": "150px",
                                "width": "200px",
                                "backgroundColor": "white",
                                "borderStyle": "solid",
                                "borderRadius": "20px",
                                "borderWidth": "thin",
                            },
                        ),
                        html.Span(id="game_name0", style={"font-size": "14px"}),
                    ],
                    style={
                        "display": "inline-block",
                        "height": "300px",
                        "width": "200px",
                        "margin": "30px",
                        "vertical-align": "top",
                        "text-align": "center",
                    },
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                html.A(
                                    html.Img(
                                        id="game_logo1",
                                        style={"height": "150px", "padding": "20px"},
                                    ),
                                    id="game_url1",
                                    target="_blank",
                                )
                            ],
                            style={
                                "display": "inline-block",
                                "height": "150px",
                                "width": "200px",
                                "backgroundColor": "white",
                                "borderStyle": "solid",
                                "borderRadius": "20px",
                                "borderWidth": "thin",
                            },
                        ),
                        html.Span(id="game_name1", style={"font-size": "14px"}),
                    ],
                    style={
                        "display": "inline-block",
                        "height": "300px",
                        "width": "200px",
                        "margin": "30px",
                        "vertical-align": "top",
                        "text-align": "center",
                    },
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                html.A(
                                    html.Img(
                                        id="game_logo2",
                                        style={"height": "150px", "padding": "20px"},
                                    ),
                                    id="game_url2",
                                    target="_blank",
                                )
                            ],
                            style={
                                "display": "inline-block",
                                "height": "150px",
                                "width": "200px",
                                "backgroundColor": "white",
                                "borderStyle": "solid",
                                "borderRadius": "20px",
                                "borderWidth": "thin",
                            },
                        ),
                        html.Span(id="game_name2", style={"font-size": "14px"}),
                    ],
                    style={
                        "display": "inline-block",
                        "height": "300px",
                        "width": "200px",
                        "margin": "30px",
                        "vertical-align": "top",
                        "text-align": "center",
                    },
                ),
            ],
            style={"display": "inline-block"},
        ),
    ],
    className="form-review",
)

admin_layout = html.Div(
    [
        html.H1("Admin Page "),
        html.Div(id="admin-page-content"),
        html.P(dcc.Link("Go to Home ", href="/"), style={"marginTop": "20px"}),
    ]
)


@app.callback(
    dash.dependencies.Output("output_players_nb_range_slider", "children"),
    [dash.dependencies.Input("players_nb_range_slider", "value")],
)
def update_output(value):
    if value[1] == 10:
        return (
            "Je recherche un jeu qui se joue à plus de "
            + format(value[0])
            + " joueur(s)"
        )
    else:
        return (
            "Je recherche un jeu qui se joue de "
            + format(value[0])
            + " à "
            + format(value[1])
            + " joueurs"
        )


@app.callback(
    dash.dependencies.Output("output_age_range_slider", "children"),
    [dash.dependencies.Input("age_range_slider", "value")],
)
def update_output(value):
    if value[1] == 14:
        return (
            "Je recherche un jeu qui se joue à partir de " + format(value[0]) + " an(s)"
        )
    else:
        return (
            "Je recherche un jeu qui se joue de "
            + format(value[0])
            + " à "
            + format(value[1])
            + " ans"
        )


@app.callback(
    [
        Output("game_url0", "href"),
        Output("game_logo0", "src"),
        Output("game_name0", "children"),
        Output("game_url1", "href"),
        Output("game_logo1", "src"),
        Output("game_name1", "children"),
        Output("game_url2", "href"),
        Output("game_logo2", "src"),
        Output("game_name2", "children"),
    ],
    [Input("submit_button", "n_clicks")],
    [State("review", "value"),],
)
def update_best_games(submit_clicks, review):
    if submit_clicks > 0:
        if review is not None and review.strip() != "":
            response = requests.post(
                f"{config.API_URL}/find_best_games", data={"review": review}
            )
            game_ids = response.json()
            selected_games = games[lambda x: x.index.isin(game_ids)]
            url_list = list(selected_games.url)
            logo_list = list(selected_games.url_img)
            name_list = list(selected_games.title)
            return (
                url_list[0],
                logo_list[0],
                name_list[0],
                url_list[1],
                logo_list[1],
                name_list[1],
                url_list[2],
                logo_list[2],
                name_list[2],
            )


@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def display_page(pathname):
    if pathname == "/":
        return home_layout
    if pathname == "/admin":
        return admin_layout
    else:
        return [
            html.Div(
                [html.Img(src="./assets/404.png", style={"width": "50%"}),],
                className="form-review",
            ),
            dcc.Link("Go to Home", href="/"),
        ]


if __name__ == "__main__":
    app.run_server(debug=config.DEBUG, host=config.HOST)
