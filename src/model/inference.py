import os
import pickle

import fasttext
import numpy as np
import pandas as pd
from dotenv import load_dotenv

from utils import (
    clean_themes,
    cleantext,
    compute_cosine_similiraty,
    get_min_age,
    get_min_max_duration,
    get_most_likely_themes,
    get_nb_players,
    vectorize_sentence,
)

pd.set_option("max_colwidth", 20000000)
load_dotenv()

PATH_DATA = os.getenv("PATH_DATA")
MODEL_PATH = os.getenv("MODEL_PATH")

file = open(os.path.join(PATH_DATA, "le.obj"), "rb")
label_encoder = pickle.load(file)
file.close()

file = open(os.path.join(PATH_DATA, "model.obj"), "rb")
clf_model = pickle.load(file)
file.close()

ft_model = fasttext.load_model(MODEL_PATH)

input = "J’aimerai un jeu collaboratif, où tous les joueurs sont des fermiers qui tente de se mettre au bio défiant les grandes industries de l’agroalimentaire."

input_cleaned = cleantext(input)
input_vectorized = vectorize_sentence(input_cleaned, ft_model).reshape(1, -1)
input_themes_prob = clf_model.predict_proba(input_vectorized)
input_themes = get_most_likely_themes(
    input_themes_prob[0], encoder=label_encoder, nb_class=10
)
print(input_themes)

df = pd.read_pickle(os.path.join(PATH_DATA, "philibert_fr_vectorized.pkl")).drop_duplicates(subset='title')

df["intersection"] = df["cleaned_themes"].apply(
    lambda x: len(list(set.intersection(set(x), set(input_themes))))
)
df_filtered = df[lambda x: x.intersection > 0]

df_filtered["cosine_similarity"] = df_filtered["vectorized_description"].apply(
    compute_cosine_similiraty, user_sentence_vectorized=input_vectorized
)

df_filtered.sort_values(by="cosine_similarity", ascending=False).head(
    10
).title
