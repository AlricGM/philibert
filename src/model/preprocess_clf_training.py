import os
import pickle
import re

import fasttext
import nltk
import numpy as np
import pandas as pd
from dotenv import load_dotenv
from sklearn.metrics import confusion_matrix as cm
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import LabelEncoder

from utils import (
    clean_themes,
    cleantext,
    get_min_age,
    get_min_max_duration,
    get_most_likely_themes,
    get_nb_players,
    vectorize_sentence,
)

load_dotenv()

PATH_DATA = os.getenv("PATH_DATA")
MODEL_PATH = os.getenv("MODEL_PATH")

pd.set_option("max_colwidth", 20000000)
pd.set_option("max_rows", 1000)

df = pd.read_pickle(os.path.join(PATH_DATA, "philibert_fr.pkl"))

ft_model = fasttext.load_model(MODEL_PATH)

df["long_description_clean"] = df["long_description"].apply(cleantext)
df["short_description_clean"] = df["short_description"].apply(cleantext)

df["description_clean"] = (
    df["short_description_clean"] + " " + df["long_description_clean"]
)
df["age_min"] = df["age"].apply(get_min_age)

df[["nb_player_min", "nb_player_max"]] = df.apply(
    lambda row: pd.Series(get_nb_players(row["nb_joueur"])), axis=1
)

df[["duration_min", "duration_max"]] = df.apply(
    lambda row: pd.Series(get_min_max_duration(row["duration"])), axis=1
)

df["vectorized_short_description"] = df["short_description_clean"].apply(
    vectorize_sentence, model=ft_model
)
df["vectorized_long_description"] = df["long_description_clean"].apply(
    vectorize_sentence, model=ft_model
)
df["vectorized_description"] = df["description_clean"].apply(
    vectorize_sentence, model=ft_model
)
df.explode(column="themes").themes.value_counts()
themes_list = [
    "Amérique",
    "Nature",
    "Animaux",
    "Western",
    "Science Fiction",
    "Construction",
    "Enquête",
    "Fantastique",
    "Antiquité",
    "Sports",
    "Navigation",
    "Histoire",
    "Renaissance",
    "Exploration",
    "Industrie",
    "Economie",
    "Commerce",
    "Littérature",
    "Horreur",
    "Pirates",
    "Mythologie",
    "Enigme",
    "Politique",
    "Médiéval",
    "Abstrait",
    "Post-Apocalypse",
    "Espace",
    "Aventure",
    "Contemporain",
    "Alimentation",
    "Superhéros",
    "Vie quotidienne",
    "Contes",
    "Guerre",
    "Géographie",
    "Europe",
    "Loisirs",
    "Asie",
    "Zombies",
    "Transports",
    "Humour",
    "Cinéma",
    "Bandes Dessinées",
    "Moyen-Orient",
    "Océan",
    "Sciences",
    "Banquise",
    "Polar",
    "Moyen Age",
    "Victorien",
    "Archéologie",
    "Dinosaures",
    "Arts",
    "Train",
    "Afrique",
    "Casino",
    "Viking",
    "Lettres",
    "Agriculture",
    "Chiffres",
    "Steampunk",
    "Cyberpunk",
    "Onirique",
    "Préhistoire",
    "Folklore",
    "Océanie",
    "Epoque moderne",
    "Religion",
    "Magie",
]

df_clf = df.explode(column="themes")[lambda x: x["themes"].isin(themes_list)]

label_encoder = LabelEncoder()
df_clf["themes_cat"] = label_encoder.fit_transform(df_clf["themes"])

X_train = np.matrix(df_clf["vectorized_description"].tolist())
y_train = df_clf["themes_cat"]
clf = MLPClassifier(random_state=1, max_iter=300).fit(X_train, y_train)

X_tot = np.matrix(df["vectorized_description"].tolist())

df["predicted_themes_proba"] = list(clf.predict_proba(X_tot))
df["predicted_themes"] = df["predicted_themes_proba"].apply(
    get_most_likely_themes, encoder=label_encoder
)
df["cleaned_themes"] = df.apply(clean_themes, axis=1)

df.to_pickle(os.path.join(PATH_DATA, "philibert_fr_vectorized.pkl"))

filehandler = open(os.path.join(PATH_DATA, "le.obj"), "wb")
pickle.dump(label_encoder, filehandler)
filehandler.close()

filehandler = open(os.path.join(PATH_DATA, "model.obj"), "wb")
pickle.dump(clf, filehandler)
filehandler.close()
