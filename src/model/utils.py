import itertools
import re

import cleantext as cltxt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity


def cleantext(text):
    """
    Converts text information into lower case clean text, without extra_spaces, numbers and punctuation
    """
    if isinstance(text, list):
        text = " ".join(text)
    return cltxt.clean(
        text,
        extra_spaces=True,
        lowercase=True,
        numbers=True,
        punct=True,
        stopwords=True,
        stp_lang="french",
    )


def get_min_age(string):
    """
    Converts text information to min age int
    """
    if isinstance(string, str):
        ages = re.findall("[0-9]+", string)
        if "mois" in string:
            ages = [int(a) / 12 for a in ages]
        else:
            ages = [int(a) for a in ages]
    else:
        ages = [0]
    return min(ages)


def get_nb_players(string):
    """
    Converts text information into min age and max age
    """
    if isinstance(string, str):
        players = re.findall("[0-9]+", string)
        if "joueur" in string:
            players = [int(p) for p in players]
        else:
            players = [0]
    else:
        players = [0]
    return pd.Series((min(players), max(players)))


def get_min_max_duration(string):
    """
    Converts text information into min and max duration
    """
    string = (
        cltxt.clean(
            string, extra_spaces=True, lowercase=True, numbers=False, punct=True
        )
        .replace("moins", "0")
        .replace("plus", "10")
    )
    durations = [
        int(d) / 60 if int(d) > 10 else int(d) for d in re.findall("[0-9]+", string)
    ]
    if not durations:
        durations = [0, 10]
    return pd.Series((min(durations), max(durations)))


def vectorize_sentence(string, model):
    """
    Converts text information into a vector
    """
    return np.array(model.get_sentence_vector(string))


def one_hot_encode_categorials(df):
    '''
    Reformat & one-hot encode columns:
    'authors', 'themes', 'mecanism' & 'language'
    '''
    l_to_concat = [df]
    for col in ['authors', 'themes', 'mecanism', 'language']:
        if col == 'editor':
            df[col] = df[col].replace(r'\n', '', regex=True)
            y = pd.get_dummies(df[col], prefix='editor')
        else:
            if col == 'language':
                df[col] = df[col].str.replace(' ', '')
                l_aux = []
                for i in range(len(df[col])):
                    l_aux.append(str(list(map(str, df[col][i].split(",")))))
                df[col] = l_aux
            df[col] = df[col].apply(literal_eval)
            y = pd.get_dummies(df[col].apply(pd.Series), prefix=col, prefix_sep='_').sum(level=0, axis=1)
        l_to_concat.append(y)
        del df[col]
    df = pd.concat(l_to_concat, axis=1)
    return df


def get_most_likely_themes(probs, encoder, nb_class=3):
    """
    Compute the most likely classes
    """
    classes = encoder.classes_
    return [x for _, x in sorted(zip(probs, classes), reverse=True)][0:nb_class]


def clean_themes(x):
    """
    Compute the intersection between predicted and
    """
    intersec = len(list(set.intersection(set(x.predicted_themes), set(x.themes))))
    if intersec == 0:
        cleaned_themes = x.predicted_themes
    else:
        cleaned_themes = list(set((x.predicted_themes + x.themes)))
    return cleaned_themes


def compute_cosine_similiraty(vectorized_string, user_sentence_vectorized=""):
    """
    Compute cosine_similarity between user sentence vector and an other one
    """
    return cosine_similarity(vectorized_string.reshape(1, -1), user_sentence_vectorized)
