import re
import os
import pandas as pd
from bs4 import BeautifulSoup
import requests
from tqdm import tqdm

unwanted_sentences = [
    "Boîte filmée -Matériel intact - Un coin abîmé - Jeu en français"
    "Boîte filmée -Matériel intact - Un coin abîmé - Jeu en anglais"
    "Boîte filmée -Matériel intact - Un coin abîmé - Jeu multilingue"
    "Boîte filmée -Matériel intact - Boîte abîmée- Jeu en français"
    "Boîte filmée -Matériel intact - Boîte abîmée- Jeu en anglais"
    "Boîte filmée -Matériel intact - Boîte abîmée- Jeu multilingue"
    "Boîte filmée -Matériel intact - Coins abîmés - Jeu en français"
    "Boîte filmée -Matériel intact - Coins abîmés - Jeu en anglais"
    "Boîte filmée -Matériel intact - Coins abîmés - Jeu multilingue"
    "Boîte filmée -Matériel intact - Boîte abîmée - Extension en français",
    "Boîte filmée -Matériel intact - Boîte abîmée - Extension en anglais",
    "Boîte filmée -Matériel intact - Boîte abîmée - Extension multilingue",
    "",
    " ",
]

dead_url = [
    "https://www.philibertnet.com/fr/summon-entertainment-corp/41960-epic-roll-seize-the-dice-684758996350.html",
    "https://www.philibertnet.com/fr/fun-consortium/24345-booster-power-tatoo-3770003539018.html",
]


def get_short_description(game_page):
    short_description = game_page.find("div", {"id": "short_description_content"})
    if short_description is None:
        short_description = ""
    else:
        if len(short_description.findAll("p")) == 0:
            short_description = short_description.text
        else:
            short_description = ". ".join(
                [p.text for p in short_description.findAll("p")]
            )
        if short_description in unwanted_sentences:
            short_description = ""
    return short_description


def get_title(game_page):
    return game_page.find("h1", {"id": "product_name"}).text


def get_duration(game_page):
    try:
        dur = game_page.find("li", {"class": "duree_partie tooltips"}).find("span").text
    except AttributeError:
        dur = None
    return dur


def get_nb_joueur(game_page):
    try:
        nb_j = game_page.find("li", {"class": "nb_joueurs tooltips"}).find("span").text
        nb_j = re.sub(r"\n", "", nb_j)
    except AttributeError:
        try:
            nb_j = game_page.find("li", {"class": "age tooltips"}).find("span").text
            nb_j = re.sub(r"\n", "", nb_j)
        except AttributeError:
            nb_j = None
    return nb_j


def get_long_description(game_page):
    if len(game_page.findAll("div", {"class": "rte"})) > 1:
        long_description = [
            par.text
            for par in game_page.findAll("div", {"class": "rte"})[1].findAll("div")
        ]
        long_description += [
            par.text
            for par in game_page.findAll("div", {"class": "rte"})[1].findAll("p")
        ]
        long_description += [
            par.text
            for par in game_page.findAll("div", {"class": "rte"})[1].findAll("li")
        ]
    else:
        long_description = []
    return [s for s in long_description if s not in unwanted_sentences]


def get_language(game_page):
    try:
        return game_page.find("li", attrs={"langue tooltips"}).text
    except AttributeError:
        return None


def get_age(game_page):
    try:
        return game_page.find("li", attrs={"age tooltips"}).text
    except AttributeError:
        return None


def get_players_rating(game_page):
    try:
        players_rating = game_page.find("li", attrs={"ukooreviews_globale_average"})
    except AttributeError:
        players_rating = None
    try:
        players_rating = players_rating.find("div", attrs={"rating-stars"}).meta[
            "content"
        ]
    except AttributeError:
        players_rating = None
    return players_rating


def get_blogs_rating(game_page):
    try:
        blogs_rating = game_page.find("li", attrs={"ukooreviews_group_2"})
    except AttributeError:
        blogs_rating = None
    try:
        blogs_rating = blogs_rating.find("div", attrs={"rating-stars"}).text
    except AttributeError:
        blogs_rating = None
    return blogs_rating


def get_mecanism(game_page):
    try:
        table_data = game_page.find("table", attrs="table-data-sheet").findAll("a")
        return [elt.text for elt in table_data if "36" in elt["href"]]
    except AttributeError:
        return None


def get_authors(game_page):
    try:
        table_data = game_page.find("table", attrs="table-data-sheet").findAll("a")
        return [elt.text for elt in table_data if "31" in elt["href"]]
    except AttributeError:
        return None


def get_editor(game_page):
    try:
        return game_page.find("tr", attrs={"editor"}).find("a").text
    except AttributeError:
        return None


def get_themes(game_page):
    try:
        table_data = game_page.find("table", attrs="table-data-sheet").findAll("a")
        return [elt.text for elt in table_data if "35" in elt["href"]]
    except AttributeError:
        return None

def get_image_url(game_page):
    return game_page.find("img", {"id": "bigpic"})['data-src']



def scraper(url):
    game_page = BeautifulSoup(requests.get(url).content, "html.parser")
    print(url)
    return pd.Series(
        (
            get_title(game_page),
            get_duration(game_page),
            get_nb_joueur(game_page),
            get_short_description(game_page),
            get_long_description(game_page),
            get_language(game_page),
            get_age(game_page),
            get_players_rating(game_page),
            get_blogs_rating(game_page),
            get_mecanism(game_page),
            get_authors(game_page),
            get_editor(game_page),
            get_themes(game_page),
            get_image_url(game_page)
        )
    )


URL = "https://www.philibertnet.com/fr/50-jeux-de-societe?id_category=50&n=36&p="
SAVE_PATH = "/Users/alricgaurier/OD/personal_project/philibert/data/"
n_init = 0
n_end = 300

for i in tqdm(range(n_init, n_end)):
    print(i)
    games_url = URL + str(i + 1)
    r = requests.get(games_url)
    soup = BeautifulSoup(r.content, "html.parser")
    url_list = soup.findAll("p", attrs={"s_title_block"})
    url_list = [game.find("a")["href"] for game in url_list]
    url_list = [url for url in url_list if url not in dead_url]
    print(len(url_list), "games being processed")
    df = pd.DataFrame(url_list, columns=["url"])
    df[
        [
            "title",
            "duration",
            "nb_joueur",
            "short_description",
            "long_description",
            "language",
            "age",
            "players_rating",
            "blogs_rating",
            "mecanism",
            "authors",
            "editor",
            "themes",
            "url_img"
        ]
    ] = df.apply(lambda row: pd.Series(scraper(row["url"])), axis=1)
    df.to_csv(os.path.join(SAVE_PATH, "_".join([str(i), "philibert_data.csv"])))
